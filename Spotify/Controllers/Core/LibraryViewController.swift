//
//  LibraryViewController.swift
//  Spotify
//
//  Created by Jorge Gimenez on 23/04/2021.
//

import UIKit

class LibraryViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
    }
    
}
