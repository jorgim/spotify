//
//  UserProfile.swift
//  Spotify
//
//  Created by Jorge Gimenez on 23/04/2021.
//

import Foundation

struct UserProfile {
    let country: String
    let display_name: String
    let email: String
    let explicit_content: [String: Int]
    let external_url: [String: String]
    let followers: [String: Codable?]
    let id: String
    let product: String
    let images: [UserImage]
}

struct UserImage: Codable {
    let url: String
}
